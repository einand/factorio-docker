FROM einand/factorio:builder AS builder
ARG version
RUN wget --connect-timeout 3 https://factorio.com/get-download/${version}/headless/linux64 -O factorio.tar.xz
RUN tar xf factorio.tar.xz
RUN rm -rv factorio/data/elevated-rails
RUN rm -rv factorio/data/quality
RUN rm -rv factorio/data/space-age
RUN pwd

FROM bitnami/minideb:latest

COPY init.sh /srv/factorio/init.sh

RUN useradd -ms /bin/bash factorio && \
    mkdir -p /srv/factorio && mkdir -p /srv/saves && \
    chown -R factorio:factorio /srv/factorio && \
    chown -R factorio:factorio /srv/saves && \
    chmod +x /srv/factorio/init.sh
COPY --from=builder factorio /srv/factorio/

USER factorio
WORKDIR /srv/factorio

ENTRYPOINT ["/srv/factorio/init.sh"]
