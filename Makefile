tag := $(shell curl -s 'https://factorio.com/api/latest-releases' | jq '.stable.headless' -r)
experimental := $(shell curl -s 'https://factorio.com/api/latest-releases' | jq '.experimental.headless' -r)


repo := factorio
platform := linux/amd64
namespace := einand
current_dir = $(shell pwd)


DOCKER_BUILDX_BUILD = \
	docker buildx build \
	--build-arg version=$(1) \
	--platform $(platform) \
	-t $(namespace)/$(repo):$(2)-$(1) \
	-t $(namespace)/$(repo):$(2) . -f $(3) \
	$(4)

run:
	echo 'nothing to do'

update-builder:
	$(call DOCKER_BUILDX_BUILD,builder,builder,Dockerfile-builder,--push)


build:
	docker buildx create --name factorio --use
	$(call DOCKER_BUILDX_BUILD,$(tag),stable,Dockerfile,--load)
	$(call DOCKER_BUILDX_BUILD,$(tag),stable-space-age,Dockerfile-space-age,--load)
	docker buildx rm factorio

pushstable:
	docker buildx create --name factorio --use
	$(call DOCKER_BUILDX_BUILD,$(tag),stable,Dockerfile,--push)
	$(call DOCKER_BUILDX_BUILD,$(tag),stable-space-age,Dockerfile-space-age,--push)
	docker buildx rm factorio

pushexperimental:
	docker buildx create --name factorio --use
	$(call DOCKER_BUILDX_BUILD,$(experimental),experimental,Dockerfile,--push)
	$(call DOCKER_BUILDX_BUILD,$(experimental),experimental-space-age,Dockerfile-space-age,--push)
	docker buildx rm factorio

push: pushstable pushexperimental

dev-bash: build
	docker run -it --rm  -v $(shell pwd)/src:/srv/app  $(namespace)/$(repo):stable-$(tag) /bin/bash

pushOnUpdate:	
	@URL="https://hub.docker.com/v2/repositories/$(namespace)/$(repo)/tags/stable-space-age-$(tag)"; \
	response=$$(curl -s -o /dev/null -w "%{http_code}" $$URL); \
	if [ $$response -eq 200 ]; then \
		echo "stable tag is up to date"; \
	else \
		$(MAKE) pushstable; \
	fi

	@URL="https://hub.docker.com/v2/repositories/$(namespace)/$(repo)/tags/experimental-space-age-$(experimental)"; \
	response=$$(curl -s -o /dev/null -w "%{http_code}" $$URL); \
	if [ $$response -eq 200 ]; then \
		echo "experimental tag is up to date"; \
	else \
		$(MAKE) pushexperimental; \
	fi

repo:
	$(info $(server)/$(repo):$(tag))

reload:
ifneq (,$(wildcard ./restart.txt))
	docker compose pull
	docker compose down
ifeq (,$(wildcard ./saves/save.zip))
		cp ./saves/save.zip ./saves/$(date +"%Y-%m-%d_%H%M%S").zip
endif
	docker compose up -d
	rm restart.txt
endif

clean:
	docker buildx rm factorio

check-tag:
	@URL="https://hub.docker.com/v2/repositories/$(namespace)/$(repo)/tags/stable-space-age-$(tag)"; \
	response=$$(curl -s -o /dev/null -w "%{http_code}" $$URL); \
	if [ $$response -eq 200 ]; then \
		echo "Taggen '$(tag)' finns i repository '$(repo)'."; \
	else \
		echo "Taggen '$(tag)' finns INTE i repository '$(repo)'."; \
	fi

	@URL="https://hub.docker.com/v2/repositories/$(namespace)/$(repo)/tags/experimental-space-age-$(experimental)"; \
	response=$$(curl -s -o /dev/null -w "%{http_code}" $$URL); \
	if [ $$response -eq 200 ]; then \
		echo "Taggen experimental '$(experimental)' finns i repository '$(repo)'."; \
	else \
		echo "Taggen experimental '$(experimental)' finns INTE i repository '$(repo)'."; \
	fi