#!/usr/bin/env bash

cd factorio

if [[ -f "/srv/saves/save.zip" ]]; then
        echo "Savegame exists!"
    else
        echo "Creating savegame!"
        ./bin/x64/factorio --create /srv/saves/save.zip
fi

./bin/x64/factorio --start-server /srv/saves/save.zip